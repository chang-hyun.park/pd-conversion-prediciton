function [pValue, observedStat, statStr, summaryMeasure, residuals] = PermForConfounderAdjustment(Y, Group, Confounder)

nGroups = numel(unique(Group));
rng("default")

% Step 1: Adjust for the confounder
if ~isempty(Confounder)
    [~,~,residuals] = regress(Y, [ones(size(Y)), Confounder]);
else
    residuals = Y;
end

% Step 2: Compute the observed test statistic using rank-sum on residuals
iGroup = double(categorical(Group, unique(Group,'stable')));
summaryMeasure.mean = NaN(1,numel(unique(iGroup)));
summaryMeasure.std = NaN(1,numel(unique(iGroup)));
for i_group = 1:numel(unique(iGroup))
    summaryMeasure.mean(i_group) = mean(residuals(iGroup==i_group));
    summaryMeasure.std(i_group) = std(residuals(iGroup==i_group));
end
if nGroups > 2
    [~,tbl] = kruskalwallis(residuals, Group,'off');
    observedStat = tbl{2,5}; % chi2
    statStr = 'Chi2';
    clear('tbl')
elseif nGroups == 2
    [~,~,stats] = ranksum(residuals(iGroup==1), residuals(iGroup==2), 'method', 'approximate');
    observedStat = stats.zval; % z
    statStr = 'Z';
    clear('stats')
end
clear('iGroup')

% Step 3: Permutation procedure
nPermutations = 1000; % Number of permutations
permutedStats = zeros(nPermutations, 1); % To store permuted statistics

for i = 1:nPermutations
    % Shuffle group labels
    shuffledGroup = Group(randperm(length(Group)));
    
    % Recompute test statistic with shuffled labels
    if nGroups > 2
        % Apply the Kruskal-Wallis test to the residuals for more than two groups
        [~,tbl] = kruskalwallis(residuals, shuffledGroup,'off');
        stat = tbl{2,5}; % chi2
        clear('tbl')
    elseif nGroups == 2
        % Apply the rank sum test to the residuals for two groups
        iGroup = double(categorical(shuffledGroup));
        [~,~,stats] = ranksum(residuals(iGroup==1), residuals(iGroup==2), 'method', 'approximate');
        stat = stats.zval; % z
        clear('iGroup','stats')
    end
    permutedStats(i) = stat;

    clear('shuffledGroup', 'stat')
end

% Step 4: Calculate p-value
if nGroups > 2 || observedStat >= 0
    greaterThanObserved = nnz(permutedStats >= observedStat);
    pValue = greaterThanObserved / nPermutations;
else
    lessThanObserved = nnz(permutedStats <= observedStat);
    pValue = lessThanObserved / nPermutations;
end
% pValue = mean(permutedStats >= observedStat);